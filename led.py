#!/usr/bin/python -w

import smbus
import time
import struct
import random

bus = smbus.SMBus(1)

# This is the address we setup in the Arduino Program
address = 0x08


class Command:
    def set(self, first, last, red, green, blue):
        buffer = struct.pack("=BHHBBBx", 2, first, last, blue, green, red)      
        for b in bytearray(buffer):
            bus.write_byte(address, b)


cmd = Command()

cmd.set(0, 144, 255, 147, 41)

while True:
    i = random.randint(0, 144)
    j = random.uniform(0.5, 1)
    cmd.set(i, i+1, 255 * j, 147*j, 41*j)
    time.sleep(random.uniform(0.1, 0.5))

